Below you shall find recommended steps on how to manufacture our PCB.

1- Firstly, films, one per layer, should be printed using the PCB design files generated in KiCAD via a laser printer and stored safely until step 2 is started. 

2- Before moving on, it is recommended to punch the holes into the marked areas in the films in order to assist with alignment later.

3- In this step, the copper should be etched and cut according to the film developed, and then coated in resist. After the resist cures, step 3 is completed.

4- In order to remove any excess copper left over in step 3, a copper solvent should be used. After the excess copper is disolved, rinse off the excess solvent.

5- Once all the layers are clean and prepped as per the above steps, alignment can be done; if step 2 was followed, one may align the layers via the hole position and inspect the components visually for any imperfections, and then if all is well, move on to step 6

6- After alignment and inspection, one can move on to the bonding stage. Using pre-impregnated epoxy sheets on either side of the sandwich of layers, along with a thin copper foil above and below this pre-impregnated layer, which follows the copper trace etchings, one can press the layers together and drill the holes marked by the film. 

7- Once the drilling and cleaning is complete, we move on to apply the silkscreen and solder mask to the surface, using the film and etchings as a guideline.

8- Finally, one can use a router to bevel and curve the board to specification and polish these surfaces.

9- Once step 8 is complete, the contruction of the PCB is complete (It is recommended to perform electrical testing in order to make sure the connections are not open).

10- We can then procede to solder on the components, specific component names can be found in the bill of materials at the end of this document.

For a bill of materials, refer to the link: https://gitlab.com/SimpleItsGordon/pihat-project-eee3088/-/blob/master/Design/BOM.csv
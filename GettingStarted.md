Guide to utilising the PiHAT:

This PiHAT is structured to use a sensor blackbox independent of the PiHAT so in order to choose an application, refer below:

1- If the main system is dependant on a motor or similar device, move on to step 2

2- If the main system utilised a limiting factor, i.e. a pressure sensor, humidity sensor etc., then move on to step 3

3- If the main system is able to output or be limited to low DC voltage, threshold specified in the main document, without distortion or error in the signal, then move on to step 4

4-  If the main system does not draw large amounts of current or cause feedback greater than the ZVD circuit can handle, then move on to step 5

5- This system can utilise this PiHAT for it's purpose of interpreting and feeding back sensor information between a motor or similar device as well as a Pi.

Note: If you are unsure about the compatibility of your system and our PiHAT, refer to the requirements and specifications document (https://gitlab.com/SimpleItsGordon/pihat-project-eee3088/-/tree/master/Requirements%20and%20Specifications), as it contains example systems that may help you understand the function and compatibility of this system. 
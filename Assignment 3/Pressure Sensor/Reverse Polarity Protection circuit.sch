EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 4 4
Title "Reverse Polarity Protection"
Date "2021-06-02"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:R_US R8
U 1 1 60B96B49
P 7500 4150
F 0 "R8" H 7432 4104 50  0000 R CNN
F 1 "100k" H 7432 4195 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 7540 4140 50  0001 C CNN
F 3 "~" H 7500 4150 50  0001 C CNN
	1    7500 4150
	-1   0    0    1   
$EndComp
$Comp
L Device:L L2
U 1 1 60B9FE2B
P 5300 3500
F 0 "L2" V 5490 3500 50  0000 C CNN
F 1 "10u" V 5399 3500 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D6.0mm_P4.00mm" H 5300 3500 50  0001 C CNN
F 3 "~" H 5300 3500 50  0001 C CNN
	1    5300 3500
	0    -1   -1   0   
$EndComp
$Comp
L Device:C C2
U 1 1 60BA0F56
P 6000 4050
F 0 "C2" H 6115 4096 50  0000 L CNN
F 1 "1u" H 6115 4005 50  0000 L CNN
F 2 "Capacitor_THT:C_Radial_D5.0mm_H5.0mm_P2.00mm" H 6038 3900 50  0001 C CNN
F 3 "~" H 6000 4050 50  0001 C CNN
	1    6000 4050
	1    0    0    -1  
$EndComp
$Comp
L Device:R_US R9
U 1 1 60BA3139
P 9000 4000
F 0 "R9" H 8932 3954 50  0000 R CNN
F 1 "75k" H 8932 4045 50  0000 R CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" V 9040 3990 50  0001 C CNN
F 3 "~" H 9000 4000 50  0001 C CNN
	1    9000 4000
	-1   0    0    1   
$EndComp
$Comp
L Transistor_FET:2N7000 Q2
U 1 1 60BA4685
P 3900 3600
F 0 "Q2" V 4242 3600 50  0000 C CNN
F 1 "2N7000" V 4151 3600 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 4100 3525 50  0001 L CIN
F 3 "https://www.onsemi.com/pub/Collateral/NDS7002A-D.PDF" H 3900 3600 50  0001 L CNN
	1    3900 3600
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6800 3500 7300 3500
Wire Wire Line
	4500 4500 6000 4500
Wire Wire Line
	5450 3500 6000 3500
Wire Wire Line
	6000 4200 6000 4500
Connection ~ 6000 4500
Wire Wire Line
	6000 4500 7500 4500
Wire Wire Line
	6000 3900 6000 3500
Connection ~ 6000 3500
Wire Wire Line
	6000 3500 6500 3500
Wire Wire Line
	7500 4300 7500 4500
Wire Wire Line
	7500 4000 7500 3800
Wire Wire Line
	7500 4000 8000 4000
Connection ~ 7500 4000
Wire Wire Line
	8000 3700 8000 3500
Wire Wire Line
	8000 3500 7700 3500
Wire Wire Line
	7500 4500 9000 4500
Connection ~ 7500 4500
Wire Wire Line
	9000 4150 9000 4500
Wire Wire Line
	9000 3850 9000 3500
Wire Wire Line
	9000 3500 8000 3500
Connection ~ 8000 3500
Text GLabel 3900 3800 3    50   Input ~ 0
PULSE(2.2u_4.4u)
Text HLabel 3700 3500 0    50   Input ~ 0
VLED
Text GLabel 4500 4500 3    50   Input ~ 0
GND
$Comp
L Diode:1N4148 D6
U 1 1 60BC3348
P 6650 3500
F 0 "D6" H 6650 3283 50  0000 C CNN
F 1 "1N4148" H 6650 3374 50  0000 C CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 6650 3325 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 6650 3500 50  0001 C CNN
	1    6650 3500
	-1   0    0    1   
$EndComp
$Comp
L Diode:1N47xxA D7
U 1 1 60BC697B
P 8000 3850
F 0 "D7" V 7954 3930 50  0000 L CNN
F 1 "1N47xxA" V 8045 3930 50  0000 L CNN
F 2 "Diode_THT:D_DO-41_SOD81_P10.16mm_Horizontal" H 8000 3675 50  0001 C CNN
F 3 "https://www.vishay.com/docs/85816/1n4728a.pdf" H 8000 3850 50  0001 C CNN
	1    8000 3850
	0    1    1    0   
$EndComp
Wire Wire Line
	4100 3500 4500 3500
$Comp
L Diode:1N4148 D5
U 1 1 60BC8778
P 4500 4050
F 0 "D5" V 4454 4130 50  0000 L CNN
F 1 "1N4148" V 4545 4130 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 4500 3875 50  0001 C CNN
F 3 "https://assets.nexperia.com/documents/data-sheet/1N4148_1N4448.pdf" H 4500 4050 50  0001 C CNN
	1    4500 4050
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 4500 4500 4200
Wire Wire Line
	4500 3900 4500 3500
Connection ~ 4500 3500
Wire Wire Line
	4500 3500 5150 3500
Text HLabel 9150 3500 2    50   Input ~ 0
Vpi
Wire Wire Line
	9000 3500 9150 3500
Connection ~ 9000 3500
$Comp
L Transistor_FET:BS250 Q3
U 1 1 60BAC1DE
P 7500 3600
F 0 "Q3" V 7842 3600 50  0000 C CNN
F 1 "BS250" V 7751 3600 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-92_Inline" H 7700 3525 50  0001 L CIN
F 3 "http://www.vishay.com/docs/70209/70209.pdf" H 7500 3600 50  0001 L CNN
	1    7500 3600
	0    -1   -1   0   
$EndComp
Text Notes 8650 6900 0    118  ~ 0
RDDPRA015
$EndSCHEMATC

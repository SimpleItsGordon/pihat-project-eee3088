EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title "Switching Voltage Regulator"
Date "2021-06-02"
Rev "0.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Diode:1N914 D1
U 1 1 60B6C0EB
P 5200 3450
F 0 "D1" V 5154 3530 50  0000 L CNN
F 1 "1N914" V 5245 3530 50  0000 L CNN
F 2 "Diode_THT:D_DO-35_SOD27_P7.62mm_Horizontal" H 5200 3275 50  0001 C CNN
F 3 "http://www.vishay.com/docs/85622/1n914.pdf" H 5200 3450 50  0001 C CNN
	1    5200 3450
	0    1    1    0   
$EndComp
$Comp
L Device:L L1
U 1 1 60B71520
P 5700 2850
F 0 "L1" V 5890 2850 50  0000 C CNN
F 1 "470u" V 5799 2850 50  0000 C CNN
F 2 "Inductor_THT:L_Radial_D6.0mm_P4.00mm" H 5700 2850 50  0001 C CNN
F 3 "~" H 5700 2850 50  0001 C CNN
	1    5700 2850
	0    -1   -1   0   
$EndComp
$Comp
L pspice:CAP C1
U 1 1 60B7366F
P 6250 3350
F 0 "C1" H 6428 3396 50  0000 L CNN
F 1 "22u" H 6428 3305 50  0000 L CNN
F 2 "Capacitor_THT:CP_Radial_D5.0mm_P2.00mm" H 6250 3350 50  0001 C CNN
F 3 "~" H 6250 3350 50  0001 C CNN
	1    6250 3350
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R1
U 1 1 60B748FD
P 6950 3300
F 0 "R1" H 7018 3346 50  0000 L CNN
F 1 "300" H 7018 3255 50  0000 L CNN
F 2 "Resistor_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P2.54mm_Vertical" H 6950 3300 50  0001 C CNN
F 3 "~" H 6950 3300 50  0001 C CNN
	1    6950 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 2850 5200 2850
Wire Wire Line
	5200 3300 5200 2850
Connection ~ 5200 2850
Wire Wire Line
	5200 2850 5550 2850
Wire Wire Line
	5850 2850 6250 2850
Wire Wire Line
	6250 3100 6250 2850
Connection ~ 6250 2850
Wire Wire Line
	6250 2850 6950 2850
Wire Wire Line
	6950 3200 6950 2850
Connection ~ 6950 2850
Wire Wire Line
	6950 2850 7350 2850
Wire Wire Line
	5200 3600 5200 3950
Wire Wire Line
	5200 3950 6250 3950
Wire Wire Line
	6250 3950 6250 3600
Wire Wire Line
	6250 3950 6950 3950
Wire Wire Line
	6950 3950 6950 3400
Connection ~ 6250 3950
Wire Wire Line
	4150 2850 3750 2850
Text GLabel 5200 3950 3    50   Input ~ 0
GND
Text HLabel 7350 2850 2    50   Input ~ 0
StrainGauge
Text HLabel 3750 2550 1    50   Input ~ 0
Motor
Wire Wire Line
	3750 2550 3750 2850
Connection ~ 3750 2850
Wire Wire Line
	3750 2850 3450 2850
Wire Wire Line
	3450 2850 3450 2550
Text HLabel 3450 2550 1    50   Input ~ 0
LED_Status
Text GLabel 4350 3950 3    50   Input ~ 0
PULSE(2.2u_4.4u)
Text GLabel 3750 3950 3    50   Input ~ 0
20V
Wire Wire Line
	4350 3150 4350 3950
Wire Wire Line
	3750 2850 3750 3950
$Comp
L Transistor_BJT:2N2219 Q1
U 1 1 60B9E168
P 4350 2950
F 0 "Q1" V 4678 2950 50  0000 C CNN
F 1 "2N2219" V 4587 2950 50  0000 C CNN
F 2 "Package_TO_SOT_THT:TO-39-3" H 4550 2875 50  0001 L CIN
F 3 "http://www.onsemi.com/pub_link/Collateral/2N2219-D.PDF" H 4350 2950 50  0001 L CNN
	1    4350 2950
	0    -1   -1   0   
$EndComp
Text Notes 8600 6900 0    118  ~ 0
MXXWEI003
$EndSCHEMATC
